<?php
//TEXTOS DE LOGIN

$idiomaLogin=array();
$idiomaLogin['login']='Login';
$idiomaLogin['senha']='Senha';
$idiomaLogin['mensagem_logout']='Desconectado do sistema';
$idiomaLogin['erroLogin']='Erro ao efetuar login';
$idiomaLogin['erroLoginInfo']='Usuário e/ou senha invalido(s)';
$idiomaLogin['erroRec']='Erro recuperar senha';
$idiomaLogin['erroRecInfo']='O email informado não foi encontrado';
$idiomaLogin['s_ip']='Seu IP';

$idiomaAdmin=array();
$idiomaAdmin['titulo_admin']='Administração HOSTMGR';
$idiomaAdmin['titulo_cliente']='Portal do cliente';

$idioma['dominios']='Domínios';

$idioma['sem_registros']="Dados não encontrados";
$idioma['nenhum_tutorial']="Nenhum tutorial encontrado para esta categoria";
$idioma['sem_registros_faturas']="No momento não existem faturas em aberto";
$idioma['sem_registros_tickets']="No momento não existem tickets de suporte em aberto";
$idioma['reg_encontrados']="Foram encontrados";
$idioma['registros']="registros!";
$idioma['mostra_de']="Exibindo registros de";
$idioma['mostra_ate']="a";
$idioma['clique_para'] = "Clique Para";
$idioma['ativar'] = "Ativar";
$idioma['desativar'] = "Desativar";

$idioma_botao['salvar'] = "salvar";
$idioma_botao['enviar_mensagem'] = "enviar mensagem";


$idioma['porcentagem']='Porcentagem';
$idioma['especie']='Espécie';
$idioma['sim']='Sim';
$idioma['nao']='Não';

$idioma['titulo_meusdados'] = "MEUS DADOS";
$idioma['titulo_meusservicos'] = "MEUS SERVIÇOS";
$idioma['titulo_contato'] = "CONTATO";
$idioma['titulo_meusservicos'] = "MEUS SERVIÇOS";
$idioma['titulo_meusdominios'] = "MEUS DOMÍNIOS";
$idioma['titulo_faturas'] = "FATURAS";
$idioma['titulo_ticketssuporte'] = "TICKETS DE SUPORTE";
$idioma['titulo_registrar'] = "REGISTRE-SE";
$idioma['titulo_carrinho'] = "Carrinho";
# modulos
$idioma['titulo_modulo_clientes'] = "CLIENTES";
$idioma['titulo_modulo_servidores'] = "SERVIDORES";
$idioma['titulo_modulo_faturas'] = "FATURAS";
$idioma['titulo_modulo_produtos'] = "PRODUTOS/SERVIÇOS";
$idioma['titulo_modulo_emails'] = "ENVIO DE EMAILS";
$idioma['titulo_modulo_pedidos'] = "PEDIDOS";
$idioma['titulo_modulo_precosdominios'] = "PREÇOS DE DOMÍNIOS";
$idioma['titulo_modulo_formasdepagamento'] = "FORMAS DE PAGAMENTO";
$idioma['titulo_modulo_promocoes'] = "PROMOÇÕES";
$idioma['titulo_modulo_opcoesconfiguraveis'] = "OPÇÕES CONFIGURÁVEIS";
$idioma['titulo_modulo_adicionaisprodutos'] = "ADICIONAIS DOS PRODUTOS";
$idioma['titulo_modulo_configuracoesgerais'] = "CONFIGURAÇÕES";
$idioma['titulo_modulo_afiliados'] = "AFILIADOS";
$idioma['titulo_modulo_baseconhecimento'] = "BASE DE CONHECIMENTO";
$idioma['titulo_modulo_templatesemails'] = "TEMPLATES DE EMAILS";
$idioma['titulo_modulo_contaspagar'] = "CONTAS / VENCIMENTOS";
$idioma['titulo_modulo_ticketssuporte'] = "TICKETS DE SUPORTE";
$idioma['titulo_modulo_areadocliente'] = "ÁREA DO CLIENTE";

# ambiente
$idioma['tipoHost']='Hospedagem Compartilhada';
$idioma['tipoRevenda']='Revenda de Hospedagem';
$idioma['tipoServer']='Servidor Dedicado / VPS';
$idioma['tipoDominio']='Domínio';
?>