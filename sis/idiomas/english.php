<?php
//TEXTOS DE LOGIN

$idiomaLogin=array();
$idiomaLogin['login']='Login';
$idiomaLogin['senha']='Senha';
$idiomaLogin['mensagem_logout']='Desconectado do sistema';
$idiomaLogin['erroLogin']='Erro ao efetuar login';
$idiomaLogin['erroLoginInfo']='Usuário e/ou senha invalido(s)';
$idiomaLogin['erroRec']='Erro recuperar senha';
$idiomaLogin['erroRecInfo']='O email informado não foi encontrado';
$idiomaLogin['s_ip']='Seu IP';


$idioma['sem_registros']="Data not found";
$idioma['nenhum_tutorial']="Nenhum tutorial encontrado para esta categoria";
$idioma['sem_registros_faturas']="No momento não existem faturas em aberto";
$idioma['sem_registros_tickets']="No momento não existem tickets de suporte em aberto";
$idioma['reg_encontrados']="Foram encontrados";
$idioma['registros']="registros!";
$idioma['mostra_de']="Exibindo registros de";
$idioma['mostra_ate']="a";
$idioma['clique_para'] = "Clique Para";
$idioma['ativar'] = "Active";
$idioma['desativar'] = "Deactive";

$idioma_botao['salvar'] = "Save";
$idioma_botao['enviar_mensagem'] = "Send Mensage";


$idioma['porcentagem']='Porcentagem';
$idioma['especie']='Espécie';
$idioma['sim']='Yes';
$idioma['nao']='No';

$idioma['titulo_meusdados'] = "MY PROFILE";
$idioma['titulo_meusservicos'] = "MY SERVICES";
$idioma['titulo_contato'] = "CONTACT US";
$idioma['titulo_meusdominios'] = "MY DOMAINS";
$idioma['titulo_faturas'] = "INVOICES";
$idioma['titulo_ticketssuporte'] = "SUPPORT TICKETS";
$idioma['titulo_registrar'] = "SIGN UP";
$idioma['titulo_carrinho'] = "Shopping Cart";

$idioma['dominios']='Domains';

# modulos
$idioma['titulo_modulo_clientes'] = "CLIENTS";
$idioma['titulo_modulo_servidores'] = "SERVERS";
$idioma['titulo_modulo_faturas'] = "INVOICES";
$idioma['titulo_modulo_produtos'] = "PRODUCTS/SERVICES";
$idioma['titulo_modulo_emails'] = "ENVIO DE EMAILS";
$idioma['titulo_modulo_pedidos'] = "ORDERS";
$idioma['titulo_modulo_opcoesconfiguraveis'] = "CONFIGURABLE OPTIONS";
$idioma['titulo_modulo_adicionaisprodutos'] = "ADICIONAIS DOS PRODUTOS";
$idioma['titulo_modulo_afiliados'] = "AFILIATES";
$idioma['titulo_modulo_baseconhecimento'] = "BASE DE CONHECIMENTO";
$idioma['titulo_modulo_ticketssuporte'] = "SUPPORT TICKETS";
$idioma['titulo_modulo_areadocliente'] = "CLIENT AREA";

# ambiente
$idioma['tipoHost']='Hospedagem Compartilhada';
$idioma['tipoRevenda']='Revenda de Hospedagem';
$idioma['tipoServer']='Dedicated Server / VPS';
$idioma['tipoDominio']='Domain';
?>