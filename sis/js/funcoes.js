/*FUNÇÃO SLIDE DESTAQUES*/
$(document).ready(function() {
	
	//busca
	$("#filtro").click(function(){
		var busca=$("#busca");
		if(busca.css("display")=='none'){
			busca.slideDown(400);
			setTimeout(function(){
				busca.css("display",'');				
			},300);
		}else{	
			busca.slideUp(400);
			setTimeout(function(){
				busca.css("display","none");				
			},300);
		}
	});
	//#########
	
	$("#txtFormaDePagamento").change(function(){
		$('#formSelectForma').submit();
	});
	
	$("#selecionarIdioma").change(function(){
		$('#formIdioma').submit();
	});
	
	
	
	/*FORM PEDIDO*/
	
	$('#txtTipoDominio_0').click(function(){
		var a =$('#espacoDominioReg');
		var b =$('#espacoDominioTrans');
		var c =$('#espacoDominioDns');
		
		a.css('display','block');
		b.css('display','none');
		c.css('display','none');
		
	});
	
	$('#txtTipoDominio_1').click(function(){
		var a =$('#espacoDominioReg');
		var b =$('#espacoDominioTrans');
		var c =$('#espacoDominioDns');
		
		a.css('display','none');
		b.css('display','block');
		c.css('display','none');
		
	});
	
	$('#txtTipoDominio_2').click(function(){
		var a =$('#espacoDominioReg');
		var b =$('#espacoDominioTrans');
		var c =$('#espacoDominioDns');
		
		a.css('display','none');
		b.css('display','none');
		c.css('display','block');
		
	});
	
	//CICLO DE PAGAMENTO
	
	$('input[name=txtCicloPagamento]').click(function(){
		
		var ciclos=Array();
		ciclos[1]='mensal';
		ciclos[3]='trimestral';
		ciclos[6]='semestral';
		ciclos[12]='anual';
		ciclos[24]='bienal';
		ciclos[36]='trienal';
		
		$(".mensal").css('display','none');
		$(".trimestral").css('display','none');
		$(".semestral").css('display','none');
		$(".anual").css('display','none');
		$(".bienal").css('display','none');
		$(".trienal").css('display','none');
		
		$('.'+ciclos[$(this).val()]).css('display','inline');
		
		
	});
	
	$("#nav").change(function(){
		window.location=$(this).val();
	});
	
	/*FORM PEDIDO*/
	
	// hospedar um site
	$('.btnConHosp').click(function(){
		window.location="produtos.php?dominio="+$('#chkDominio').val();							
	});
	
	$('.chkDisp').click(function(){
		window.location="carrinho.php?TD=1&dominio="+$('#chkDominio').val();							
	});
	
	$('#formChecar').submit(function(){
		window.location="carrinho.php?TD=1&dominio="+$('#chkDominio').val();							
		return false;
	});
	
	
	$('.btTransferir').click(function(){
		window.location="carrinho.php?TD=2&dominio="+$('#chkDominio').val();							
	});
	
	$('#txtCep').change(function(){
		 $('#txtEndereco').attr('value','Buscando endereço, Aguarde...');
		 $.ajax({
            url : 'modulos/buscaCep.php?cep='+$(this).val(),
            success: function(data){
				data = data.split('|');
				
				$('#txtEstado').attr('value',data[0]);
				$('#txtCidade').attr('value',data[1]);
				$('#txtBairro').attr('value',data[2]);
				
				if(data[4]!=''){
					$('#txtEndereco').attr('value',data[3]+' '+data[4]);
					$('#txtNumero').focus();
				}else{
					$('#txtEndereco').attr('value','');
					$('#txtEndereco').focus();	
				}
				
            }
        });
	});
	
	//MASCARAS
	
	$("#txtCep").mask("99999-999");
	$("#txtTelefone").mask("(99) 9999-9999?9");
	
});