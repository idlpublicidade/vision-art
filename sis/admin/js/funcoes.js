/*FUNÇÃO SLIDE DESTAQUES*/
$(document).ready(function() {
	
	$(".lista, .lista2").mouseover(function(){
		$(this).addClass("over");

	});
	
	$(".lista, .lista2").mouseout(function(){
			$(this).removeClass("over");

	});
	
	var marcar = "N";
	
	//busca
	$("#filtro").click(function(){
		var busca=$("#busca");
		if(busca.css("display")=='none'){
			busca.slideDown(400);
			setTimeout(function(){
				busca.css("display",'');				
			},300);
		}else{	
			busca.slideUp(400);
			setTimeout(function(){
				busca.css("display","none");				
			},300);
		}
	});
	//#########
	
	$(".lista, .lista2").click(function(){
			$(this).removeClass("over");
			
			var checkbox = $(this).find("input[type='checkbox']");
			
			if(checkbox.attr("checked")){
				checkbox.attr('checked',false);
				$(this).removeClass("click");
				$(this).removeClass("warning");
				marcar = "N";
				$("#checkPadrao").removeAttr("checked");
			}else{
				checkbox.attr('checked',true);
				$(this).addClass("click");
				$(this).addClass("warning");
			}
	});
	
	//MARCAR TUDO
	
	$("#checkPadrao").click(function(){
		if(marcar=="N"){
			$("input[type='checkbox']").attr('checked',true);
			$(".lista, .lista2").addClass("click");
			marcar="S";
			
		}else{
			$("input[type='checkbox']").attr('checked',false);
			$(".lista, .lista2").removeClass("click");
			marcar="N";
		}
	});
	
	
	//confirmação de exclusão
	
	$(".dstvPagamento").click(function(){
		var confirmar = confirm("DESATIVAR\nTem certeza que deseja desativar? As configurações serão perdidas");
		
		return confirmar;
	});
	
	/*STATUS*/
	
	
	$(".status").click(function(){
		var objAjax=$("#status"+$(this).attr("rel"));
		objAjax.html("Processando...");
		objAjax.load($(this).attr("href"));
		return false;
	});

	//NAVEGADOR PRODUTOS
	
	$('#formListaProdutos').submit(function(){
		
		window.location="clientes.php?acao=produtos&PID="+$("#txtListaProdutos").val();
		
		return false;
	});
	
	$("#txtListaProdutos").change(function(){
		$('#formListaProdutos').submit();
	});
	
	// NAVEGAR DOMINIOS
	
	$('#formListaDominios').submit(function(){
		
		window.location="clientes.php?acao=dominios&DID="+$("#txtListaDominios").val();
		
		return false;
	});
	
	$("#txtListaDominios").change(function(){
		$('#formListaDominios').submit();
	});
	
	/* BUSCA DE INFORMAÇÕES (BUSCA DO TOPO) */
	
	var buscaTopo=0;
	
	$("#txtBuscaTopo1").focus(function(){
		if($(this).val()=="Buscar"){
			$(this).val("");
		}
	});
	
	$("#txtBuscaTopo1").blur(function(){
		if($(this).val()==""){
			$(this).val("Buscar");
		}
	});
	
	$("#txtBuscaTopo2").focus(function(){
		if($(this).val()=="Buscar"){
			$(this).val("");
		}
	});
	
	$("#txtBuscaTopo2").blur(function(){
		if($(this).val()==""){
			$(this).val("Buscar");
		}
	});
	
	var form = $(".formBuscatopo");
	
	form.submit(function(){
		
		var termoBuscaTopo=$("#txtBuscaTopo1").val();
		
		if(termoBuscaTopo=='' || termoBuscaTopo==null){
			termoBuscaTopo=$("#txtBuscaTopo2").val();
		}
		
		if(termoBuscaTopo=='' || termoBuscaTopo==null){
			return false;	
		}
		
		if(termoBuscaTopo!='Buscar'){
			/*if(buscaTopo==0){
				$('body').append('<div style="height:auto; width:100%; position: absolute; z-index:220; top:0"><div class="limite row" style="margin:0 auto; position:relative"><div id="resultadoBusca" class="span10"><p class="icone">&nbsp;</p><a href="" class="fechar">Fechar</a><div id="resp"><div class="loading">&nbsp;</div></div></div></div></div>');
				
				$("body").append('<div id="overlay" style="height: '+$(document).height()+'px;"></div>');
				$("#overlay").css("opacity", 0.5);
				$('html, body').animate({scrollTop:0}, 'slow');
				buscaTopo=1;
			}
			
			var resp = $("body").find("#resp");
			*/
			
			
			$.ajax({
				type: "POST",
				url: "buscarapida.php",
				data: { busca: termoBuscaTopo },
				success:function(retorno) {
					//resp.html(retorno);
					$('#buscaModal').find('.modal-body').html(retorno);
					$('#buscaModal').modal({
						keyboard: false
					})
				}
			});
			
			//$('.fechar').click(function(){$('#resultadoBusca').remove(); $('#overlay').remove(); buscaTopo=0; return false;});
		
		}
		return false;
	});
	
	/*function closeBusca(){
		$("#buscaTopo").("#resp");
	}*/
	
	/* FIM BUSCA TOPO */
	
	$('.addLinha').click(function(){
		
		var contar=$('#contar').val();
		contar++;
			
		$('#contar').val(contar);
			
		if(contar&1){css='lista';}else{css='lista2';}
		
		$('#tabelaListaOpcoes').append($('#tabelaListaOpcoes tr:last').clone());
		$('#tabelaListaOpcoes tr:last').removeClass();
		$('#tabelaListaOpcoes tr:last').addClass(css);
		$('#tabelaListaOpcoes tr:last').find("input").val('');
				
		return false;
	});
	
	
	
	$('.addProduto').click(function(){
		
		var l1 = $('div.prod1').html();		
		$('div.prod1').removeClass();
		$(this).before('<div class="prod1">'+l1+'</div>');
						
		return false;
	});
	
	
	/*ALTERAR STATUS DO TICKET*/
	
	$('#txtTicketStatus').change(function(){
		$.ajax({
			type: "POST",
			url: "estrutura/ticketssuporte/status.php",
			data: { status: $(this).val(), ticket:$('#txtTicket').val() },
			success:function(retorno) {
				if(retorno!=''){alert(retorno);}
			}
		});
	});
	
	$('#txtTicketPrioridade').change(function(){
		$.ajax({
			type: "POST",
			url: "estrutura/ticketssuporte/prioridade.php",
			data: { prioridade: $(this).val(), ticket:$('#txtTicket').val() },
			success:function(retorno) {
				if(retorno!=''){alert(retorno);}
			}
		});
	});
	
	$('#txtTicketDepartamento').change(function(){
		$.ajax({
			type: "POST",
			url: "estrutura/ticketssuporte/departamento.php",
			data: { departamento: $(this).val(), ticket:$('#txtTicket').val() },
			success:function(retorno) {
				if(retorno!=''){alert(retorno);}
			}
		});
	});
	
	$('.saveNotas').click(function(){
		$.ajax({
			type: "POST",
			url: "widgets/notas.php",
			data: { notas: $("#txtNotas").val() },
			success:function(retorno) {
				resp.html(retorno);
			}
		});
	});
	
	$('.saveObs').click(function(){
		$.ajax({
			type: "POST",
			url: "estrutura/clientes/obs.php",
			data: { obs: $("#obs").val(),cliente: $("#obs").attr('name') },
			success:function(retorno) {
				resp.html(retorno);
			}
		});
		
		return false;
	});
	
	/*ENVIAR EMAILS EM MASSA (FILTRO)*/

		$('#txtFiltroEmail').change(function(){
			$('#txtPara').load('estrutura/emails/carregarDestinatarioMassa.php?filtro='+$(this).val());
		});

	/*FIM*/
	
	$("#nav").change(function(){
		window.location=$(this).val();
	});
	
	$("#loadResposta").change(function(){
		$('#txtResposta').load('js/loadResposta.php?busca='+$(this).val());
	});
	
	
	//alert('teste');
	/*permissoes admin*/
	$("input:radio[name='txtNivel']").change(function(){
		if(Number($("input[name='txtNivel']:checked").val()) > 5){
			$('#niveisPersonalizados').css('display','block');
		}else{
			$('#niveisPersonalizados').css('display','none');
		}
	});
	/*fim*/
	
	
	
	$(".lnkExcluir").click(function(){	
		obj=$(this);
		$('#dialog-confirm').html(exIco+"Tem certeza que deseja Excluir os Registros Selecionados?");
		$("#dialog-confirm").dialog({
			resizable: false,
			height:140,
			modal: true,
			buttons: {
				"Excluir": function() {
				$( this ).dialog( "close" );
					
					window.location=obj.attr('href');
				},
				"Cancelar": function() {
					$( this ).dialog( "close" );
					return false
				}
			}
		});
			
		return false;
		
	});
	
	$(".btExcluir").click(function(){	
		obj=$(this);
			$('#dialog-confirm').html(exIco+"Tem certeza que deseja Excluir os Registros Selecionados?");
			$("#dialog-confirm").dialog({
				resizable: false,
				height:140,
				modal: true,
				buttons: {
					"Excluir": function() {
					$( this ).dialog( "close" );
						$('#formLista').submit();
					},
					"Cancelar": function() {
						$( this ).dialog( "close" );
						return false
					}
				}
			});
			
			return false;
		
	});
	
	
});

function statusClick(rel,href){
		var objAjax=$("#status"+rel);
		objAjax.html("Processando...");
		objAjax.load(href);
		return false;
}
function cancelarPedido(id) {
	window.location = "estrutura/pedidos/processar.php?acao=cancelar&pedido="+id;
}
function pedidoPendente(id) {
	window.location = "estrutura/pedidos/processar.php?acao=pendente&pedido="+id;
}