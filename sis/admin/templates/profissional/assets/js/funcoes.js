/*FUNÇÃO SLIDE DESTAQUES*/
$(document).ready(function() {
	
	$(".lista, .lista2").mouseover(function(){
		$(this).addClass("over");

	});
	
	$(".lista, .lista2").mouseout(function(){
			$(this).removeClass("over");

	});
	
	var marcar = "N";
	
	//busca
	$("#filtro").click(function(){
		var busca=$("#busca");
		if(busca.css("display")=='none'){
			busca.slideDown(400);
			setTimeout(function(){
				busca.css("display",'');				
			},300);
		}else{	
			busca.slideUp(400);
			setTimeout(function(){
				busca.css("display","none");				
			},300);
		}
	});
	//#########
	
	$(".tabList tbody input[type='checkbox']").click(function(){
					
			var obj = $(this).parent().parent().parent().parent();
		
			var td = obj.find("td");
			
			var tdClass = td.attr('class');
			
			if(tdClass=='warning'){

				td.removeClass("warning");
				
				marcar = "N";

			}else{
				td.addClass("warning");
			}
	});
	
	//MARCAR TUDO
	
	$("#checkPadrao").click(function(){
		
		var obj = $(this).parent().parent().parent().parent().parent().parent().find('tbody');
				
		if(marcar=="N"){
			obj.find(".warning input[type='checkbox']").click();
			obj.find("input[type='checkbox']").click();
			marcar="S";
			
		}else{
			
			
			
			obj.find("input[type='checkbox']").click();
			marcar="N";
		}
	});
	
	
	//confirmação de exclusão
	
	$(".dstvPagamento").click(function(){
		
		obj=$(this);
		
		$('#modalDialog').find('.modal-body').html("Tem certeza que deseja desativar esse modulo? As configurações serão perdidas");
		$('#modalDialog').find('.cnf').html('Desativar');
		$('#modalDialog').find('.cnf').attr('href',obj.attr('href'));
		$('#modalDialog').modal({
			keyboard: true		
		})
		
		return false;

	});
	
	/*STATUS*/
	
	
	$(".status").click(function(){
		var objAjax=$("#status"+$(this).attr("rel"));
		objAjax.html("Processando...");
		objAjax.load($(this).attr("href"));
		return false;
	});

	//NAVEGADOR PRODUTOS
	
	$('#formListaProdutos').submit(function(){
		
		window.location="clientes.php?acao=produtos&PID="+$("#txtListaProdutos").val();
		
		return false;
	});
	
	$("#txtListaProdutos").change(function(){
		$('#formListaProdutos').submit();
	});
	
	// NAVEGAR DOMINIOS
	
	$('#formListaDominios').submit(function(){
		
		window.location="clientes.php?acao=dominios&DID="+$("#txtListaDominios").val();
		
		return false;
	});
	
	$("#txtListaDominios").change(function(){
		$('#formListaDominios').submit();
	});
	
	/* BUSCA DE INFORMAÇÕES (BUSCA DO TOPO) */
	
	var buscaTopo=0;
	
	$("#txtBuscaTopo1").focus(function(){
		if($(this).val()=="Buscar..."){
			$(this).val("");
		}
	});
	
	$("#txtBuscaTopo1").blur(function(){
		if($(this).val()==""){
			$(this).val("Buscar...");
		}
	});
		
	var form = $(".formBuscatopo");
	
	form.submit(function(){
		
		$('.search-form').css('margin-top', '-60px');
		
		var termoBuscaTopo=$("#txtBuscaTopo1").val();
				
		if(termoBuscaTopo=='' || termoBuscaTopo==null){
			return false;	
		}
		
		if(termoBuscaTopo!='Buscar...'){

			$.ajax({
				type: "POST",
				url: "buscarapida.php",
				data: { busca: termoBuscaTopo },
				success:function(retorno) {
					$("#txtBuscaTopo1").val('');
					$('#buscaModal').find('.modal-body').html(retorno);
					$('#buscaModal').modal({
						keyboard: true
					})
				}
			});
		
		}
		return false;
	});
	
	
	/* FIM BUSCA TOPO */
	
	
	$('#botaoFinalizar').click(function(){	
		obj=$(this);
		
		$('#modalDialog').find('.modal-body').html("Finalizar excluirá essa conta de seu servidor. Deseja continuar?");
		$('#modalDialog').find('.cnf').html('Executar Comando');
		$('#modalDialog').find('.cnf').attr('href',obj.attr('href'));
		$('#modalDialog').modal({
			keyboard: true		
		})

		return false;
		
	});
	
	
	
	$(".lnkExcluir").click(function(){	
		obj=$(this);
		
		$('#modalDialog').find('.modal-body').html("Tem certeza que deseja Excluir esse Registro?");
		$('#modalDialog').find('.cnf').html('Excluir');
		$('#modalDialog').find('.cnf').attr('href',obj.attr('href'));
		$('#modalDialog').modal({
			keyboard: true		
		})

		return false;
		
	});
	
	$(".btExcluir").click(function(){
								   					   
		obj=$(this);
		
		$('#modalDialog').find('.modal-body').html("Tem certeza que deseja Excluir os Registros Selecionados?");
		
		$('#modalDialog').find('.cnf').click(function(){
			$('#formLista').submit();
		});
		
		$('#modalDialog').modal({
			keyboard: true		
		})

		return false;						   
								   		
	});
	
	
	
	$('.addLinha').click(function(){
		
		var contar=$('#contar').val();
		contar++;
			
		$('#contar').val(contar);
			
		if(contar&1){css='lista';}else{css='lista2';}
		
		$('#tabelaListaOpcoes').append($('#tabelaListaOpcoes tr:last').clone());
		$('#tabelaListaOpcoes tr:last').removeClass();
		$('#tabelaListaOpcoes tr:last').addClass(css);
		$('#tabelaListaOpcoes tr:last').find("input").val('');
				
		return false;
	});
	
	
	
	$('.addProduto').click(function(){
		
		var l1 = $('div.prod1').html();		
		$('div.prod1').removeClass();
		$(this).before('<div class="prod1">'+l1+'</div>');
						
		return false;
	});
	
	
	/*ALTERAR STATUS DO TICKET*/
	
	$('#txtTicketStatus').change(function(){
		$.ajax({
			type: "POST",
			url: "estrutura/ticketssuporte/status.php",
			data: { status: $(this).val(), ticket:$('#txtTicket').val() },
			success:function(retorno) {
				if(retorno!=''){alert(retorno);}
			}
		});
	});
	
	$('#txtTicketPrioridade').change(function(){
		$.ajax({
			type: "POST",
			url: "estrutura/ticketssuporte/prioridade.php",
			data: { prioridade: $(this).val(), ticket:$('#txtTicket').val() },
			success:function(retorno) {
				if(retorno!=''){alert(retorno);}
			}
		});
	});
	
	$('#txtTicketDepartamento').change(function(){
		$.ajax({
			type: "POST",
			url: "estrutura/ticketssuporte/departamento.php",
			data: { departamento: $(this).val(), ticket:$('#txtTicket').val() },
			success:function(retorno) {
				if(retorno!=''){alert(retorno);}
			}
		});
	});
	
	$('.saveNotas').click(function(){
		$.ajax({
			type: "POST",
			url: "widgets/notas.php",
			data: { notas: $("#txtNotas").val() },
			success:function(retorno) {
				resp.html(retorno);
			}
		});
	});
	
	$('.saveObs').click(function(){
		$.ajax({
			type: "POST",
			url: "estrutura/clientes/obs.php",
			data: { obs: $("#obs").val(),cliente: $("#obs").attr('name') },
			success:function(retorno) {
				resp.html(retorno);
			}
		});
		
		return false;
	});
	
	/*ENVIAR EMAILS EM MASSA (FILTRO)*/

		$('#txtFiltroEmail').change(function(){
			$('#txtPara').load('estrutura/emails/carregarDestinatarioMassa.php?filtro='+$(this).val());
		});

	/*FIM*/
	
	$("#nav").change(function(){
		window.location=$(this).val();
	});
	
	$("#loadResposta").change(function(){
		$('#txtResposta').load('js/loadResposta.php?busca='+$(this).val());
	});
	
	
	//alert('teste');
	/*permissoes admin*/
	$("input:radio[name='txtNivel']").change(function(){
		if(Number($("input[name='txtNivel']:checked").val()) > 5){
			$('#niveisPersonalizados').css('display','block');
		}else{
			$('#niveisPersonalizados').css('display','none');
		}
	});
	/*fim*/
	
});

function statusClick(rel,href){
		var objAjax=$("#status"+rel);
		objAjax.html("Processando...");
		objAjax.load(href);
		return false;
}
function cancelarPedido(id) {
	window.location = "estrutura/pedidos/processar.php?acao=cancelar&pedido="+id;
}
function pedidoPendente(id) {
	window.location = "estrutura/pedidos/processar.php?acao=pendente&pedido="+id;
}