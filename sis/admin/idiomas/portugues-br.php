<?php
//TEXTOS DE LOGIN

$idiomaLogin=array();
$idiomaLogin['login']='Login';
$idiomaLogin['senha']='Senha';
$idiomaLogin['mensagem_logout']='Desconectado do sistema';
$idiomaLogin['erroLogin']='Erro ao efetuar login';
$idiomaLogin['erroLoginInfo']='Usuário e/ou senha invalido(s)';
$idiomaLogin['erroLoginToken']='Token invalido';
$idiomaLogin['s_ip']='Seu IP';

$idiomaAdmin=array();
$idiomaAdmin['titulo_admin']='Administração HOSTMGR';

$idioma['sem_registros']="Dados não encontrados";
$idioma['reg_encontrados']="Foram encontrados";
$idioma['registros']="registros!";
$idioma['mostra_de']="Exibindo registros de";
$idioma['mostra_ate']="a";
$idioma['clique_para'] = "Clique Para";
$idioma['ativar'] = "Ativar";
$idioma['desativar'] = "Desativar";

$idioma_botao['salvar'] = "Salvar";
$idioma_botao['enviar_mensagem'] = "enviar mensagem";
$idioma['titulo_modulo_banimentoip']='Banimento de IP\'s';

$idioma['porcentagem']='Porcentagem';
$idioma['especie']='Espécie';
$idioma['sim']='Sim';
$idioma['nao']='Não';

$idioma['meses']=array('','Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro');

# modulos
$idioma['titulo_modulo_projetos'] = "PROJETOS";
$idioma['titulo_modulo_clientes'] = "CLIENTES";
$idioma['titulo_modulo_configurarcaptcha'] = "CONFIGURAR CAPTCHA";
$idioma['titulo_modulo_servidores'] = "SERVIDORES";
$idioma['titulo_modulo_faturas'] = "FATURAS";
$idioma['titulo_modulo_produtos'] = "PRODUTOS/SERVIÇOS";
$idioma['titulo_modulo_emails'] = "ENVIO DE EMAILS";
$idioma['titulo_modulo_pedidos'] = "PEDIDOS";
$idioma['titulo_modulo_precosdominios'] = "PREÇOS DE DOMÍNIOS";
$idioma['titulo_modulo_formasdepagamento'] = "FORMAS DE PAGAMENTO";
$idioma['titulo_modulo_promocoes'] = "PROMOÇÕES";
$idioma['titulo_modulo_opcoesconfiguraveis'] = "OPÇÕES CONFIGURÁVEIS";
$idioma['titulo_modulo_adicionaisprodutos'] = "ADICIONAIS DOS PRODUTOS";
$idioma['titulo_modulo_configuracoesgerais'] = "CONFIGURAÇÕES";
$idioma['titulo_modulo_baseconhecimento'] = "BASE DE CONHECIMENTO";
$idioma['titulo_modulo_templatesemails'] = "TEMPLATES DE EMAILS";
$idioma['titulo_modulo_contaspagar'] = "CONTAS / VENCIMENTOS";
$idioma['titulo_modulo_ticketssuporte'] = "TICKETS DE SUPORTE";
$idioma['titulo_modulo_administradores'] = "ADMINISTRADORES";
$idioma['titulo_modulo_afiliados'] = "AFILIADOS";
$idioma['titulo_modulo_atualizacoes'] = "CHECAR ATUALIZAÇÕES";
$idioma['titulo_modulo_licenca'] = "DADOS DA LICENÇA";
$idioma['titulo_modulo_php'] = "VERSÃO DO PHP";
$idioma['titulo_modulo_extras'] = "MODULOS EXTRAS";
$idioma['titulo_modulo_contas'] = "CONTAS/PRODUTOS";
$idioma['titulo_modulo_dominios'] = "DOMÍNIOS";
$idioma['titulo_modulo_logsistema'] = "LOG DO SISTEMA";
$idioma['titulo_modulo_relatorios'] = "RELATÓRIOS";
$idioma['titulo_modulo_downloads'] = "DOWNLOADS";
$idioma['titulo_modulo_registrantesdominios'] = "REGISTRANTES DE DOMÍNIOS";
$idioma['titulo_modulo_tarefas'] = "TAREFAS";

# ambiente
$idioma['tipoHost']='Hospedagem Compartilhada';
$idioma['tipoRevenda']='Revenda de Hospedagem';
$idioma['tipoServer']='Servidor Dedicado / VPS';
$idioma['tipoOutro']='Outro';
$idioma['tipoDominio']='Domínio';



$idioma['widgets']['notas']='Minhas Notas';
$idioma['widgets']['atividadesClientes']='Atividades de Clientes';
$idioma['widgets']['atividadesAdministradores']='Atividades de Administradores';
$idioma['widgets']['contas']='Contas em aberto';
$idioma['widgets']['tickets']='Tickets de Suporte';
$idioma['widgets']['faturamento']='Faturamento';
$idioma['widgets']['graficoFaturamento']='Gráfico de Rendimentos';
$idioma['widgets']['tarefas']='Tarefas';

?>