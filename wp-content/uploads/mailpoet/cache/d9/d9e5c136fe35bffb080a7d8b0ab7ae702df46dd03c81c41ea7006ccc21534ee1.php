<?php

/* limit.html */
class __TwigTemplate_12b9cb377bac84dc2743057022da694b76cb1b2bf0a795cdfc2b802eda8cba7b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layout.html", "limit.html", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"wrap mailpoet-about-wrap\">
  <h1>";
        // line 6
        echo twig_escape_filter($this->env, sprintf($this->env->getExtension('MailPoet\Twig\I18n')->translate("You've reached the %d subscribers limit!"), (isset($context["limit"]) ? $context["limit"] : null)), "html", null, true);
        echo "</h1>

  <p class=\"about-text\">
    ";
        // line 9
        echo twig_escape_filter($this->env, sprintf($this->env->getExtension('MailPoet\Twig\I18n')->translate("Our free version is limited to 2000 subscribers."), (isset($context["limit"]) ? $context["limit"] : null)), "html", null, true);
        echo "
  </p>

  <img
    src=\"http://i2.wp.com/www.mailpoet.com/wp-content/uploads/2015/05/sad-cat.gif?resize=500%2C212\"
    alt=\"sad-cat\"
    width=\"500\"
    height=\"212\"
  />

  <h3>";
        // line 19
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Immediately, you can:");
        echo "</h3>
  <ul class=\"ul-disc\">
    <li>";
        // line 21
        echo twig_escape_filter($this->env, sprintf($this->env->getExtension('MailPoet\Twig\I18n')->translate("Delete unconfirmed subscribers to have less than %d subscribers."), (isset($context["limit"]) ? $context["limit"] : null)), "html", null, true);
        echo "</li>
    <li>";
        // line 22
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Install the Premium plugin if you have purchased it.");
        echo "</li>
    <li>
      <a
        href=\"";
        // line 25
        echo admin_url("admin.php?page=mailpoet-premium");
        echo "\"
        class=\"button-primary\"
      >
        ";
        // line 28
        echo $this->env->getExtension('MailPoet\Twig\I18n')->translate("Check out the Premium");
        echo "
      </a>
    </li>
  </ul>
</div>
";
    }

    public function getTemplateName()
    {
        return "limit.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 28,  69 => 25,  63 => 22,  59 => 21,  54 => 19,  41 => 9,  35 => 6,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "limit.html", "/home/vision/public_html/wp-content/plugins/mailpoet/views/limit.html");
    }
}
