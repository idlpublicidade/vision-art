# Translation of Plugins - ManageWP Worker - Development (trunk) in Dutch
# This file is distributed under the same license as the Plugins - ManageWP Worker - Development (trunk) package.
msgid ""
msgstr ""
"PO-Revision-Date: 2016-11-04 09:18:23+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: GlotPress/2.4.0-alpha\n"
"Language: nl\n"
"Project-Id-Version: Plugins - ManageWP Worker - Development (trunk)\n"

#. Author of the plugin/theme
msgid "ManageWP"
msgstr "ManageWP"

#. Description of the plugin/theme
msgid "ManageWP Worker plugin allows you to manage your WordPress sites from one dashboard. Visit <a href=\"https://managewp.com\">ManageWP.com</a> for more information."
msgstr "ManageWP Worker plugin stelt je in staat om je WordPress sites vanuit één dashboard te beheren. Bezoek <a href=\"https://managewp.com\">ManageWP.com</a> voor meer informatie."

#. #-#-#-#-#  tmp-worker.pot (ManageWP - Worker 4.1.17)  #-#-#-#-#
#. Plugin URI of the plugin/theme
#. #-#-#-#-#  tmp-worker.pot (ManageWP - Worker 4.1.17)  #-#-#-#-#
#. Author URI of the plugin/theme
msgid "https://managewp.com"
msgstr "https://managewp.com"

#. Plugin Name of the plugin/theme
msgid "ManageWP - Worker"
msgstr "ManageWP - Worker"

#: src/MWP/EventListener/PublicRequest/AutomaticLogin.php:95
msgid "User <strong>%s</strong> could not be found."
msgstr "Gebruiker <strong>%s</strong> kon niet worden gevonden."

#: src/MWP/EventListener/PublicRequest/AutomaticLogin.php:89
msgid "The automatic login token is invalid. Please check if this website is properly connected with your dashboard, or, if this keeps happening, contact support."
msgstr "De automatische login token is ongeldig. Controleer of deze website juist is gekoppeld met je dashboard. Indien dit blijft gebeuren, neem dan contact op met support."

#: src/MWP/EventListener/PublicRequest/AutomaticLogin.php:78
msgid "The automatic login token was already used. Please try again, or, if this keeps happening, contact support."
msgstr "De automatische login token is al gebruikt. Probeer het opnieuw. Indien dit blijft gebeuren, neem dan contact op met support."

#: src/MWP/EventListener/PublicRequest/AutomaticLogin.php:76
msgid "The automatic login token has expired. Please try again, or, if this keeps happening, contact support."
msgstr "De automatische login token is verlopen. Probeer het opnieuw. Indien dit blijft gebeuren, neem dan contact op met support."

#: src/MWP/EventListener/PublicRequest/AutomaticLogin.php:74
msgid "The automatic login token is invalid. Please try again, or, if this keeps happening, contact support."
msgstr "De automatische login token is ongeldig. Probeer het opnieuw. Indien dit blijft gebeuren, neem dan contact op met support."