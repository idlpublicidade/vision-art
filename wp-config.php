<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */

// define('FORCE_SSL_ADMIN', true);

define('WP_SITEURL', 'http://localhost');
define('WP_HOME', 'http://localhost');

/** O nome do banco de dados do WordPress */
define('DB_NAME', 'vision');

/** MySQL database username */
define('DB_USER',  'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',       '*cLK4W#MAI@AkgoFEjqGlMt8qiFF9fJOSu*FW%PQeNYmF6kqrIyDQ2U@GRWlOG%d');
define('SECURE_AUTH_KEY',       'hAM)cW4kRsjjWz5GPBEcT)%N774)tZRdzm3mFMN7)(00xn*0TeyYiQ38ls36SMjf');
define('LOGGED_IN_KEY',       'W)5uFopI40PA@*Nlu@uqfcWrv2%2RdS%pYOvqX1g*gmrs670HpfXK36Eih%ZEb04');
define('NONCE_KEY',       '@T7piVtUw5R)qjId@rhsWkJHnIFLw62Nj^U(%whQON5eiw#XPY^KZ5MPCA&vFdSi');
define('AUTH_SALT',       'tb6!pVh@w@vmbsauY^4yiIrRJ)^lggbL9@eh^WR7DoeJrC7yFbAt%(ab*C1U3%i^');
define('SECURE_AUTH_SALT',       'J@44fuigMnKEv0crHYI)Pd4O&oyOpbkPi@gjKlf!(ICWO%foGg@X*pCABgfp55Z1');
define('LOGGED_IN_SALT',       'SJA@fpPifQLJ9)sqz5#^#NOL@EQ8RaKZ&Lija@@44q)BQHpcAD63xLi!ivUv&eWp');
define('NONCE_SALT',       'd3BpQn7Hz9dOcV1)!oZVuh*KJ&fgLuv4YUyXI9DuZhw(#6wi&jBBndtDh)lBEXgn');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', 'pt_BR');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
